app.controller('brandController', function($scope, $controller,$http, brandService) {

	$controller('baseController',{$scope:$scope});//{key:value}
	
	

	$scope.findAll = function() {
		brandService.findAll().success(function(response) {
			$scope.list = response;
			// console.log(response);
		});
	}

	$scope.findPage = function(page, size) {
		brandService.findPage(page, size).success(function(response) {
			$scope.list = response.rows;
			$scope.paginationConf.totalItems = response.total
			// console.log(response);
		});
	}

	$scope.save = function() {

		var obj;

		if ($scope.entity.id != null) {
			obj = brandService.update($scope.entity);
		} else {
			obj = brandService.add($scope.entity);
		}

		obj.success(function(response) {

			if (response.success) {
				// 刷新列表
				$scope.reloadList();

			} else {

				alert(response.message);
			}
			// console.log(response);
		});
	}
	/*
	 * $scope.add=function(){
	 * 
	 * $http.post('../brand/add.do',$scope.entity).success(function(response){
	 * 
	 * if(response.success){ //刷新列表 $scope.reloadList();
	 * 
	 * }else{
	 * 
	 * alert(response.message); } //console.log(response); }); }
	 */

	/*
	 * $scope.update=function(){
	 * 
	 * $http.post('../brand/update.do',$scope.entity).success(function(response){
	 * 
	 * if(response.success){ //刷新列表 $scope.reloadList();
	 * 
	 * }else{
	 * 
	 * alert(response.message); } //console.log(response); });
	 * 
	 *  }
	 */

	$scope.findOne = function(id) {

		brandService.findOne(id).success(function(response) {

			$scope.entity = response;// 对象带了id
		});
	}

	$scope.del = function() {
		brandService.del($scope.selectIds).success(function(response) {

			if (response.success) {
				// 刷新列表
				$scope.reloadList();

			} else {

				alert(response.message);
			}
		});
	}

	$scope.searchEntity = {}
	$scope.search = function(page, size) {
		brandService.search(page, size, $scope.searchEntity).success(
				function(response) {
					$scope.list = response.rows;
					$scope.paginationConf.totalItems = response.total
					// console.log(response);
				});
	}
});