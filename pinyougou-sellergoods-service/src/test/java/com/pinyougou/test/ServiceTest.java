package com.pinyougou.test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pinyougou.mapper.TbBrandMapper;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;

import entity.PageResult;

public class ServiceTest {
	
	@Test
	public void testFindAll(){
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext-*.xml");
		
		BrandService brandService = ctx.getBean(BrandService.class);
//		List<TbBrand> list = brandService.findAll();
//		System.out.println(list);
		
		PageResult result = brandService.findPage(1, 10);
		System.out.println(result);
		for (Object obj : result.getRows()) {
			System.out.println(obj);
		}
	}

}
