package com.pinyougou.test;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pinyougou.mapper.TbBrandMapper;
import com.pinyougou.pojo.TbBrand;

public class MapperTest {
	
	@Test
	public void testFindAll(){
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring/applicationContext-dao.xml");
		
		TbBrandMapper brandMapper = ctx.getBean(TbBrandMapper.class);
		
		TbBrand brand = brandMapper.selectByPrimaryKey(1L);
		System.out.println(brand.getName()+":"+brand.getFirstChar());
	}

}
