app.controller('baseController', function($scope) {

	// 分页控件配置
	$scope.paginationConf = {

		currentPage : 1,
		totalItems : 10,
		itemsPerPage : 10,
		perPageOptions : [ 10, 20, 30, 40, 50 ],
		onChange : function() {
			// alert(1)
			// //重新加载
			$scope.reloadList();

		}
	};
	
	$scope.reloadList = function() {
		$scope.search($scope.paginationConf.currentPage,
				$scope.paginationConf.itemsPerPage);
	};

	$scope.selectIds = [];
	$scope.updateSelectIds = function($event, id) {
		if ($event.target.checked) {
			$scope.selectIds.push(id);
		} else {
			var index = $scope.selectIds.indexOf(id);
			$scope.selectIds.splice(index, 1);
		}

	}
	
	$scope.jsonToString=function(jsonstr,key){
		
		//console.log(jsonstr)
		//console.log(key)
		var retStr = "";
		var json = JSON.parse(jsonstr);
		
		for(var i=0;i<json.length;i++){
//			{"id":3,"text":"三星"}
//			console.log(json[i])
			var value = json[i][key];
			if(i<json.length-1){
				retStr += value + ","
			}else{
				retStr += value 
			}
			
		}
		
		return retStr;
		
	}

});