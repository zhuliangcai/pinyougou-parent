 //控制层 
app.controller('goodsController' ,function($scope,$controller ,itemCatService ,uploadService ,goodsService,typeTemplateService){	
	
	$controller('baseController',{$scope:$scope});//继承
	$scope.entity = {'goods':{},'goodsDesc':{'itemImages':[],'customAttributeItems':[],'specificationItems':[]},'itemList':[]}
    
	//查找，key 和 value 在  list中是否存在
	$scope.searchObjectByKey=function(list,key,keyValue){
		//[{"attributeName":"网络","attributeValue":["移动3G"]},{"attributeName":"机身内存","attributeValue":["16G"]}]}
		for(var i=0;i<list.length;i++){
			if(list[i][key]==keyValue){
				return list[i];//存在
			}
		}
		return null;//不存在
	}
	
	
	$scope.createItemList=function(){
		//"specificationItems":[{"attributeName":"网络","attributeValue":["移动4G","联通4G","电信4G"]},
		//{"attributeName":"机身内存","attributeValue":["64G","128G"]}]
		$scope.entity.itemList=[{spec:{},'price':0,'num':9999,'status':0,'isDefault':0}];
		var items = $scope.entity.goodsDesc.specificationItems;
		for(var i=0;i<items.length;i++){
			$scope.entity.itemList = addColumn($scope.entity.itemList,items[i].attributeName,items[i].attributeValue);
		}
	}
	
	addColumn = function(list,columnName,conlumnValues){
		var newList=[];
		
		for(var i=0;i<list.length;i++){
			var oldRow = list[i];
			for(var j=0;j<conlumnValues.length;j++){
				var newRow = JSON.parse(JSON.stringify(oldRow));//深克隆
				newRow.spec[columnName]=conlumnValues[j];
				newList.push(newRow);
			}
		}
		return newList;
		
	}
	
	
	$scope.updateSpecAttribute=function($event,attributeName,attributeValue){
		
		//console.log('$event.target.checked:'+ $event.target.checked);
		
		//查找 "attributeName" 网络  在 $scope.entity.goodsDesc.specificationItems 里面是否存在
		var items = $scope.entity.goodsDesc.specificationItems;
		
		//console.log(items)
		var object = $scope.searchObjectByKey(items,"attributeName",attributeName);
		
		if(object!=null){//1存在
			if($event.target.checked){//勾选
				object.attributeValue.push(attributeValue);
				
			}else{//取消勾选
				//alert(2)
				var index = object.attributeValue.indexOf(attributeValue) //获取下标
				object.attributeValue.splice(index,1) //移除一个
				//当 attributeValue 长度为0,整个对象也需要被移除
				if(object.attributeValue.length==0){
					var objIndex = $scope.entity.goodsDesc.specificationItems.indexOf(object)
					$scope.entity.goodsDesc.specificationItems.splice(objIndex,1)
				}
				
			}
			
			
		}else{//2不存在
			
			$scope.entity.goodsDesc.specificationItems.push({"attributeName":attributeName,"attributeValue":[attributeValue]})
		}
		
	}
	
	$scope.selectItemCat1List=function(){
		itemCatService.findByParentId(0).success(function(response){
			
			$scope.itemCat1List=response
		})
	}
	
	$scope.$watch('entity.goods.category1Id',function(newValue,oldValue){
		//alert(newValue+"-"+oldValue)
		if(newValue!=null){
			itemCatService.findByParentId(newValue).success(function(response){
				
				$scope.itemCat2List=response
			})
		}
		
		
	})
	
	$scope.$watch('entity.goods.category2Id',function(newValue,oldValue){
		//alert(newValue+"-"+oldValue)
		if(newValue!=null){
			itemCatService.findByParentId(newValue).success(function(response){
				
				$scope.itemCat3List=response
			})
		}
		
	})
	$scope.$watch('entity.goods.category3Id',function(newValue,oldValue){
		//alert(newValue+"-"+oldValue)
		if(newValue!=null){
			itemCatService.findOne(newValue).success(function(response){
				
				$scope.entity.goods.typeTemplateId=response.typeId
			})
		}
		
	})
	$scope.$watch('entity.goods.typeTemplateId',function(newValue,oldValue){
		//alert(newValue+"-"+oldValue)
		if(newValue!=null){
			typeTemplateService.findOne(newValue).success(function(response){
				$scope.typeTemplate=response;
				$scope.brandIds=JSON.parse($scope.typeTemplate.brandIds);//字符串转json对象
				
				$scope.entity.goodsDesc.customAttributeItems=JSON.parse($scope.typeTemplate.customAttributeItems);//字符串转json对象
			})
		
			typeTemplateService.findSpecList(newValue).success(function(response){
				$scope.specList=response
			})
		}
		
		
	})
	
	
	
	//读取列表数据绑定到表单中  
	$scope.findAll=function(){
		goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(id){				
		goodsService.findOne(id).success(
			function(response){
				$scope.entity= response;					
			}
		);				
	}
	
	//保存 
	/*$scope.save=function(){				
		var serviceObject;//服务层对象  				
		if($scope.entity.id!=null){//如果有ID
			serviceObject=goodsService.update( $scope.entity ); //修改  
		}else{
			serviceObject=goodsService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}
			}		
		);				
	}*/
	$scope.add=function(){				
//		var serviceObject;//服务层对象  				
//		if($scope.entity.id!=null){//如果有ID
//			serviceObject=goodsService.update( $scope.entity ); //修改  
//		}else{
//			serviceObject=goodsService.add( $scope.entity  );//增加 
//		}		
		
		$scope.entity.goodsDesc.introduction= editor.html();
		//alert($scope.entity.goodsDesc.introduction);
		
		goodsService.add( $scope.entity  ).success(
			function(response){
				if(response.success){
					//重新查询 
					alert(response.message);
				}else{
					alert(response.message);
				}
			}		
		);			
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	$scope.image_entity={}
	$scope.uploadFile=function(){
		uploadService.uploadFile().success(function(response){
			$scope.image_entity.url=response.message
		});
	}
	$scope.add_image_entity=function(){
		$scope.entity.goodsDesc.itemImages.push($scope.image_entity);
		$scope.image_entity={}
	}
	$scope.remove_image_entity=function(index){
		$scope.entity.goodsDesc.itemImages.splice(index,1)
	}
	
    
});	
