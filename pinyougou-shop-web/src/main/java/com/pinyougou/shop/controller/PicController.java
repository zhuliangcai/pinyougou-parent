package com.pinyougou.shop.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import entity.Result;
import util.FastDFSClient;

@RestController
@RequestMapping("/pic")
public class PicController {
	
	@Value("${FILE_SERVER_URL}")
	private String FILE_SERVER_URL;
	
	@RequestMapping("/upload")
	public Result uploadPic(MultipartFile pic){
		
		try {
			byte[] bytes = pic.getBytes();
			String originalFilename = pic.getOriginalFilename();
			String extName = originalFilename.substring(originalFilename.lastIndexOf(".")+1);
			
			FastDFSClient client = new FastDFSClient("classpath:config/fdfs_client.conf");
			
			String uploadFile = client.uploadFile(bytes, extName);
			String url = FILE_SERVER_URL + uploadFile;
			
			return new Result(true, url);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Result(false, "上传失败");
		}
		
	}

}
