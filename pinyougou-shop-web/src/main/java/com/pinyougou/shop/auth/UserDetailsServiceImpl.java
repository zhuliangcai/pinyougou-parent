package com.pinyougou.shop.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.pinyougou.pojo.TbSeller;
import com.pinyougou.sellergoods.service.SellerService;

public class UserDetailsServiceImpl implements UserDetailsService {

	private SellerService sellerService; 
	
	public void setSellerService(SellerService sellerService) {
		this.sellerService = sellerService;
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username+"经过了此认证类");
		TbSeller tbSeller = sellerService.findOne(username);
		if(tbSeller==null){
			return null;
		}
		//状态值：  0：未审核   1：已审核   2：审核未通过   3：关闭
		if(!"1".equals(tbSeller.getStatus())){
			return null;
		}
		List<GrantedAuthority> authoritie = new ArrayList<>();
		authoritie.add(new SimpleGrantedAuthority("ROLE_SELLER"));
		
		return new User(username,tbSeller.getPassword(),authoritie);
	}

}
